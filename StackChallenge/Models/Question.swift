//
//  QuestionsPayload.swift
//  StackChallenge
//
//  Created by Daniel Hopkins on 4/19/19.
//  Copyright © 2019 Hoppities. All rights reserved.
// To parse the JSON, add this file to your project and do:
//
//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// Questions
// 1. How do I ignore certain fields I don't care about?

struct Question: Codable {
    var tags: [String] = [
        "javascript",
        "decimal",
        "fractions",
        "rational-numbers"
    ]
    var owner: Owner = Owner()
    var isAnswered: Bool = true
    var viewCount = 100, acceptedAnswerID = 14783976, answerCount = 11, score: Int = 17
    var lastActivityDate = 1555731234, creationDate = 1360372448, lastEditDate = 1360372448, questionID: Int = 1360372448
    var link: String = "https://stackoverflow.com/questions/14783869/convert-a-decimal-number-to-a-fraction-rational-number"
    var title: String = "Convert a decimal number to a fraction / rational number And it's very a long thing that needs to wrap somewhere"
    var body: String = "<p>I am creating a demo for send message function. My application has 2 screens:</p>\n\n<ul>\n<li><p>Screen 1 is a contact list. Users can choose many contacts from contact list then go to screen 2.</p></li>\n<li><p>Screen 2 is composed message screen. It has 2 parts:</p>\n\n<ul>\n<li>Part 1 is a recipient list. It will render the name of all recipient. I design it as a container. Each recipient as a button. This container allows users to input into it.</li>\n<li>Part 2 is a message to send.</li>\n</ul></li>\n</ul>\n\n<p>Please see my image:</p>\n\n<p><img src=\"https://i.stack.imgur.com/RGf8v.png\" alt=\"enter image description here\"></p>\n\n<p>I have a problem when the recipient list is too long. My container does not wrap it. It overflows to the right. And it doesn't input anything. Do you have any idea?</p>\n"
    
    public init() {
        
    }
    
//    var tags: [String]
//    var owner: Owner
//    var isAnswered: Bool
//    var viewCount, acceptedAnswerID, answerCount, score: Int
//    var lastActivityDate, creationDate, lastEditDate, questionID: Int?
//    var link: String
//    var title: String
    
//    public init(tags: [String], isAnswered: Bool, answerCount: Int, score: Int, title: String, questionID: Int) {
//        self.tags = tags;
//        self.isAnswered = isAnswered;
//        self.answerCount = answerCount;
//        self.score = score;
//        self.title = title;
//        self.questionID = questionID;
//    }
    
    enum CodingKeys: String, CodingKey {
        case tags, owner
        case isAnswered = "is_answered"
        case viewCount = "view_count"
        case acceptedAnswerID = "accepted_answer_id"
        case answerCount = "answer_count"
        case score
        case lastActivityDate = "last_activity_date"
        case creationDate = "creation_date"
        case lastEditDate = "last_edit_date"
        case questionID = "question_id"
        case link, title, body
    }
}

struct Owner: Codable {
    var reputation: Int = 11043
    var userID: Int = 975097
    var userType: String = "registered"
    var acceptRate: Int = 82
    var profileImage: String = "https://www.gravatar.com/avatar/dbbc1b2bc18739858cfa3b7ddcba7362?s=128&d=identicon&r=PG"
    var displayName: String = "Anderson Green"
    var link: String = "https://stackoverflow.com/users/975097/anderson-green"
    
    public init() {
        
    }
    
    enum CodingKeys: String, CodingKey {
        case reputation
        case userID = "user_id"
        case userType = "user_type"
        case acceptRate = "accept_rate"
        case profileImage = "profile_image"
        case displayName = "display_name"
        case link
    }
}
