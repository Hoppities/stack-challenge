// To parse the JSON, add this file to your project and do:
//
//   let answersResponse = try? newJSONDecoder().decode(AnswersResponse.self, from: jsonData)

import Foundation

struct AnswersResponse: Codable {
    let items: [Answer]
    let hasMore: Bool
    let quotaMax, quotaRemaining: Int
    
    enum CodingKeys: String, CodingKey {
        case items
        case hasMore = "has_more"
        case quotaMax = "quota_max"
        case quotaRemaining = "quota_remaining"
    }
}

struct Answer: Codable {
    var owner: Owner = Owner()
    var isAccepted: Bool = true
    var score: Int = 7
    var lastActivityDate: Int = 1556061015
    var lastEditDate: Int? = 1556061015
    var creationDate: Int = 1556061015
    var answerID: Int = 38989655
    var questionID: Int = 17380845
    var body: String = "<p>If the TypeScript compiler knows that the type of variable is string then this works:</p>\n\n<pre><code>let colorName : string = \"Green\";\nlet color : Color = Color[colorName];\n</code></pre>\n\n<p>Otherwise you should explicitly convert it to a string (to avoid compiler warnings):</p>\n\n<pre><code>let colorName : any = \"Green\";\nlet color : Color = Color[\"\" + colorName];\n</code></pre>\n\n<p>At runtime both solutions will work.</p>\n"
    
    public init() {
        
    }
    
    enum CodingKeys: String, CodingKey {
        case owner
        case isAccepted = "is_accepted"
        case score
        case lastActivityDate = "last_activity_date"
        case lastEditDate = "last_edit_date"
        case creationDate = "creation_date"
        case answerID = "answer_id"
        case questionID = "question_id"
        case body
    }
}
