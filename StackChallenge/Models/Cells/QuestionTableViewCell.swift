//
//  QuestionTableViewCell.swift
//  StackChallenge
//
//  Created by Daniel Hopkins on 4/19/19.
//  Copyright © 2019 Hoppities. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class QuestionTableViewCell: UITableViewCell {
    
    var title: UILabel = UILabel()
    var tagsList: UILabel = UILabel()
    var voteText: UILabel = UILabel()
    var votes: UILabel = UILabel()
    var isAnswered: Bool = false;
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Question
        // What is standard practice for organizing this?
        // This seems messy
        
        let byTheNumbersView: UIView = UIView()
        self.contentView.addSubview(byTheNumbersView)

        byTheNumbersView.backgroundColor = UIColor(red:0.91, green:0.98, blue:0.94, alpha:1.0)
        byTheNumbersView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalTo(self)
            make.width.equalTo(50)
        }

        byTheNumbersView.addSubview(voteText)
        byTheNumbersView.addSubview(votes)

        voteText.textColor = UIColor(red:0.35, green:0.35, blue:0.35, alpha:1.0)
        voteText.text = "Score".uppercased()
        voteText.font = voteText.font.withSize(8)
        voteText.snp.makeConstraints { (make) in
            make.centerX.equalTo(byTheNumbersView)
            make.top.equalTo(byTheNumbersView).offset(18)
            make.bottom.equalTo(votes.snp.top)
        }

        votes.textColor = UIColor(red:0.35, green:0.35, blue:0.35, alpha:1.0)
        votes.snp.makeConstraints { (make) in
            make.centerX.equalTo(byTheNumbersView)
            make.top.equalTo(voteText.snp.bottom)
            make.bottom.equalTo(byTheNumbersView)
        }
        
        let questionDetailsView: UIView = UIView()
        self.contentView.addSubview(questionDetailsView)

        questionDetailsView.snp.makeConstraints { (make) in
            make.left.equalTo(byTheNumbersView.snp.right).offset(5)
            make.bottom.top.equalTo(self).offset(5)
            make.right.equalTo(self).offset(-5)
        }
        
        questionDetailsView.addSubview(title)
//        questionDetailsView.addSubview(tagsList)
        
        title.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.bold)
        title.numberOfLines = 2
        title.lineBreakMode = NSLineBreakMode.byWordWrapping
        title.textColor = UIColor(red:0.22, green:0.45, blue:0.71, alpha:1.0)
        title.snp.makeConstraints { (make) in
            make.top.right.left.bottom.equalTo(questionDetailsView).inset(5)
//            make.bottom.equalTo(tagsList.snp.top).priority(600);
        }
        
        // For the life of me, I cannot get this layout to work
        
//        tagsList.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.medium)
//        tagsList.textColor = UIColor.gray
//        tagsList.snp.updateConstraints { (make) in
//            make.right.left.bottom.equalTo(questionDetailsView).offset(5)
//
//            make.top.equalTo(title.snp.bottom).priority(10)
//        }
        
    }

}
