//
//  AnswerTableViewCell.swift
//  StackChallenge
//
//  Created by Daniel Hopkins on 5/3/19.
//  Copyright © 2019 Hoppities. All rights reserved.
//

import UIKit
import SDWebImage

class AnswerTableViewCell: UITableViewCell {
    
    var dataSource: Answer? = nil
    
    let profilePhoto: UIImageView = UIImageView()
    let profileName: UILabel = UILabel()
    
    let metaContainerView: UIView = UIView()
    let answerContainer: UIView = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        let answer: Answer = dataSource!
        
        self.contentView.addSubview(answerContainer)
        self.contentView.addSubview(metaContainerView)
        
        self.setupAnswerMeta(answer: answer)
        self.setupAnswer(answer: answer)
        
    }
    
    private func setupAnswerMeta(answer: Answer) {
        
        metaContainerView.snp.makeConstraints { (make) in
            make.top.right.left.equalTo(self.contentView)
            make.bottom.equalTo(answerContainer.snp.top)
        }
        
        // profile photo
        profilePhoto.sd_setImage(with: URL(string: answer.owner.profileImage))
        
        metaContainerView.addSubview(profilePhoto)
        
        profilePhoto.snp.makeConstraints { (make) in
            make.height.equalTo(35)
            make.width.equalTo(35);
            make.centerY.equalTo(metaContainerView)
            make.left.equalTo(metaContainerView).offset(10)
        }
        
        // profile name
        profileName.text = answer.owner.displayName
        
        metaContainerView.addSubview(profileName)
        
        profileName.snp.makeConstraints { (make) in
            make.left.equalTo(profilePhoto.snp.right).offset(10)
            make.right.equalTo(metaContainerView).offset(10)
            make.centerY.equalTo(metaContainerView)
        }
    }
    
    private func setupAnswer(answer: Answer) {
        let body: UITextView = UITextView()
        body.attributedText = answer.body.htmlToAttributedString
        body.isScrollEnabled = false
        
        answerContainer.snp.makeConstraints { (make) in
            make.height.equalTo(100)
            make.left.right.bottom.equalTo(self.contentView)
            make.top.equalTo(metaContainerView.snp.bottom)
        }
        
        answerContainer.addSubview(body)
        body.snp.makeConstraints { (make) in
//            make.height.equalTo(body.frame.size)
            make.edges.equalTo(answerContainer)
        }
    }

}
