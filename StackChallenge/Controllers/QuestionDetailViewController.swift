//
//  QuestionDetailViewController.swift
//  StackChallenge
//
//  Created by Daniel Hopkins on 4/21/19.
//  Copyright © 2019 Hoppities. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import SnapKit

class QuestionDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var question: Question = Question()
    var answers: [Answer] = []
    let stackService: StackService = StackService()
    let answersList: UITableView = UITableView()
    
    let answersCellIdentifier = "answersCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = String(question.questionID)

//        stackService.getAnswers(id: question.questionID, completion: { (results) in
//            self.answers = results.items
//        })
        
        self.answers.append(Answer())
        
        self.view.backgroundColor = UIColor.white
        
        let questionParentView: UIView = UIView()
        let answersListParent: UIView = UIView()
        
        // add parent containers
        self.view.addSubview(questionParentView)
        self.view.addSubview(answersListParent)
        
        questionParentView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.bottom.equalTo(answersListParent)
        }
        
        answersListParent.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(self.view)
            make.top.equalTo(questionParentView)
        }
        
        let textView: UITextView = UITextView()
        textView.attributedText = question.body.htmlToAttributedString
        textView.isScrollEnabled = false
        questionParentView.addSubview(textView)
        
        answersListParent.addSubview(answersList)
        
        textView.snp.makeConstraints { (make) in
            make.edges.equalTo(questionParentView).inset(5)
        }
        
        self.answersList.register(AnswerTableViewCell.self, forCellReuseIdentifier: self.answersCellIdentifier)
        
        self.answersList.snp.makeConstraints { (make) in
            make.edges.equalTo(answersListParent)
        }
        
        self.answersList.dataSource = self
        self.answersList.delegate = self
        
        self.answersList.layoutMargins = UIEdgeInsets.zero
        self.answersList.separatorInset = UIEdgeInsets.zero
        
        // add a uiview at the bottom to get rid of the extra lines?
        // per https://stackoverflow.com/questions/1369831/eliminate-extra-separators-below-uitableview
        self.answersList.tableFooterView = UIView()
        
        self.answersList.reloadData()
        
    }    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.answersCellIdentifier, for: indexPath) as! AnswerTableViewCell
        
        cell.dataSource = answers[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
}


