//
//  ListViewController.swift
//  StackChallenge
//
//  Created by Daniel Hopkins on 4/19/19.
//  Copyright © 2019 Hoppities. All rights reserved.
//

import UIKit

// Questions
// 1. Is it really the best way to go to pass a string as a cell identifier?

class ListViewController: UITableViewController {
    
    let stackService: StackService = StackService()
    var questions: Array<Question> = []
    var selectedQuestion: Question? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set the title
        self.navigationItem.title = "Questions"
        
        // register the custom cell
        self.tableView.register(QuestionTableViewCell.self, forCellReuseIdentifier: "QuestionTableViewCell")
        
        // set the layout margin and inset to zero
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600
        
        // add a uiview at the bottom to get rid of the extra lines?
        // per https://stackoverflow.com/questions/1369831/eliminate-extra-separators-below-uitableview
        self.tableView.tableFooterView = UIView()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
//        stackService.getQuestions { (results) in
//            self.questions = results.items
//            self.tableView.reloadData()
//        }
        
        self.questions.append(Question())
        self.tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionTableViewCell", for: indexPath) as! QuestionTableViewCell

        // Configure the cell...
        let question: Question = questions[indexPath.row]
        cell.isAnswered = question.isAnswered
        cell.votes.text = String(question.score)
        cell.title.text = question.title
        cell.tagsList.text = question.tags.joined(separator: ", ")
        
        cell.layoutMargins = UIEdgeInsets.zero

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailView = QuestionDetailViewController()
        detailView.question = questions[indexPath.row]
        self.navigationController?.pushViewController(detailView, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showQuestionDetails" {
            let vc = segue.destination as! QuestionDetailViewController
            vc.question = selectedQuestion!
        }
    }

}
