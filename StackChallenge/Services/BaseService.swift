//
//  BaseService.swift
//  rb directory
//
//  Created by Daniel Hopkins on 1/19/18.
//  Copyright © 2018 Hoppities. All rights reserved.
//

import Foundation
import Alamofire

extension DateFormatter {
    static let iso8601Full: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.calendar = Calendar(identifier: .iso8601)
        //        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        //        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter
    }()
}

class BaseService: NSObject {
    let decoder = JSONDecoder()
    let encoder = JSONEncoder()
    
    override public init() {
        
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
        encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
        
        super.init()
        
    }
    
    /// Decode Data from API calls to any Entities that conform to Codable
    public func decodeResponse<T: Codable>(_ data: Data, entity: T.Type) throws ->  T {
        let decodedData = try self.decoder.decode(entity, from: data)
        return decodedData
    }
    
    /// Encode any Entities that conform to Codable to JSON Data
    public func encodeEntity<T: Codable>(_ entity: T) throws -> Data {
        let encodedData = try self.encoder.encode(entity)
        return encodedData
    }
    
}
