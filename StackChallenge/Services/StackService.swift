//
//  StackService.swift
//  StackChallenge
//
//  Created by Daniel Hopkins on 4/19/19.
//  Copyright © 2019 Hoppities. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class StackService : BaseService {
    let API_BASE: String = "https://api.stackexchange.com/2.2/"
    
    func getQuestions(completion: @escaping (_ result: QuestionsResponse)->()) {
        
        let params: Parameters = [
            "accepted": "True",
            "answers": 4,
            "closed": "False",
            "site": "stackoverflow",
            "key": "fX)CJWlUEmOfXsv)B*I8aQ((",
            "filter": "!L_Zm1rmoFv_c97PSf8AHA5"
        ]
        
        Alamofire.request(API_BASE + "search/advanced", method: .get, parameters: params).response {
            response in
            if let data = response.data {
                do {
                    let stackResponse: QuestionsResponse = try self.decoder.decode(QuestionsResponse.self, from: data)
                    completion(stackResponse)
                    print("Count: " + String(stackResponse.items.count))
                } catch {
                    print(error)
                }
                
            }
            else {
                print("failed")
            }
        }
    }
    
    func getAnswers(id: Int, completion: @escaping (_ result: AnswersResponse)->()) {
        
        let params: Parameters = [
            "key": "fX)CJWlUEmOfXsv)B*I8aQ((",
            "filter": "!SWK)GbIE7OUj.HuZ2q"
        ]
        
        Alamofire.request("\(API_BASE)questions/\(id)/answers", method: .get, parameters: params).response {
            response in
            if let data = response.data {
                do {
                    let stackResponse: AnswersResponse = try self.decoder.decode(AnswersResponse.self, from: data)
                    completion(stackResponse)
                    print("Count: " + String(stackResponse.items.count))
                } catch {
                    print(error)
                }
                
            }
            else {
                print("failed")
            }
        }
    }
}
